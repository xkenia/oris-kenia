package cz.vutbr.fit.tam.oris;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.*;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/** 
 * Fragment pro zobrazení závodů na mapě.
 * Zobrazí závody, které jsou aktuálně vypsané v seznamu.
 */
public class OrisMapFragment extends Fragment {
	
	private static GoogleMap mMap;
	MapView mMapView;
	private View mainView;
	private static Double latitude, longitude;
	private static Bundle eventsInfoBundle;
	
	/** Konstruktor */
	public OrisMapFragment(){
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
		
		mainView = inflater.inflate(R.layout.fragment_map, container, false);
		
	    if (container == null) {
	        return null;
	    }
	    
	    eventsInfoBundle = this.getArguments();
	    
	    // nastaveni latitude a longitude na střed ČR pro zazoomování
	    latitude = 49.80;
	    longitude = 15.47;
	            
	    if (mMap == null) {
	    	mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	    	setUpMap();
	    } else {
	    	GoogleMap newmap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	    	mMap = newmap;
	    }
	    
	    // zoom na střed
	    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
	                    longitude), 6));

	    return mainView;
	}
		
	/**
	 * Nastaví body v mapě podle GPS souřadnic závodů.
	 */
	private void setUpMap() {
		// tlačítko na posunutí na aktuální pozici (podle GPS)
	    mMap.setMyLocationEnabled(true);
	    
	    // vyčtení předaných informací o závodech
	    double[] latArray = eventsInfoBundle.getDoubleArray("lat");
	    double[] lonArray = eventsInfoBundle.getDoubleArray("lon");
	    String[] nameArray = eventsInfoBundle.getStringArray("name");
	    String[] clubArray = eventsInfoBundle.getStringArray("club");
	    
	    if (latArray != null) {
	    	if(MainActivity.DEBUG)
	    		Log.i("bundle info", nameArray[0] +  " " + clubArray[0] + " " + String.valueOf(latArray[0]) + " " + String.valueOf(lonArray[0]));
	
	    	// nastavení Markeru
	    	MarkerOptions mo = new MarkerOptions();
	    	mo.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_lampion));
	    	
	    	// zobrazení všech závodů, které nemají GPS souřadnice nastavené na 0.0
		    for(int i=0; i<latArray.length;i++ ){
		    	if (latArray[i] != 0.0 && lonArray[i] != 0.0) {
			    	mo.position(new LatLng(latArray[i], lonArray[i])).title(nameArray[i]).snippet(clubArray[i]);
			    	mMap.addMarker(mo);
		    	}
		    }
	    } else {
	    	Toast.makeText(this.getActivity().getApplicationContext(), "Žádné závody s GPS souřadnicemi v kalendáři", Toast.LENGTH_SHORT).show();
	    }
	  
	    // zoom na střed mapy
	    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,
	            longitude), 6));
	}


	@Override
	public void onDestroyView() {
	
	    Fragment f = (Fragment) getFragmentManager().findFragmentById(R.id.map);        
	    if (f != null) {
	    	try {
	    		getFragmentManager().beginTransaction().remove(f).commit();
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	}
	    }
	    super.onDestroyView();        
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    setUpMap();
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	}
	
	@Override
	public void onLowMemory() {
	    super.onLowMemory();
	}
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setHasOptionsMenu(true);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.action_refresh).setVisible(false);
	    menu.findItem(R.id.action_filter).setVisible(false);
	    menu.findItem(R.id.action_order_by).setVisible(false);
	    menu.findItem(R.id.action_order).setVisible(false);
	    super.onPrepareOptionsMenu(menu);
	}

}
