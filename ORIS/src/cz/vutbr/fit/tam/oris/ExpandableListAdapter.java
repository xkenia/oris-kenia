package cz.vutbr.fit.tam.oris;

import java.util.List;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseAdapter {
	
	private Context mContext;
	private List<Event> mCalendarList;
	
	public ExpandableListAdapter(Context context, List<Event> events) {
		this.mContext = context;
		this.mCalendarList = events;
	}

	public int getGroupCount() {
		return this.mCalendarList.size();
	}

	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	public Event getGroup(int groupPosition) {
		return this.mCalendarList.get(groupPosition);
	}

	public Event getChild(int groupPosition, int childPosition) {
		return null;
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

    @Override
    public int getCount() {
        return this.mCalendarList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mCalendarList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
	public boolean hasStableIds() {
		return false;
	}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.row, null);
        }

        TextView tvDate = (TextView) convertView.findViewById(R.id.row_date);
        tvDate.setText(getGroup(position).date_str);
        TextView tvName = (TextView) convertView.findViewById(R.id.row_name);
        tvName.setText(getGroup(position).name);
        TextView tvSport = (TextView) convertView.findViewById(R.id.row_sport);
        tvSport.setText(getGroup(position).sport_str);
        TextView tvOrg = (TextView) convertView.findViewById(R.id.row_organizer);
        tvOrg.setText(getGroup(position).organizer);
        TextView tvLevel = (TextView) convertView.findViewById(R.id.row_level);
        tvLevel.setText(getGroup(position).level_str);
        TextView tvDiscipline = (TextView) convertView.findViewById(R.id.row_discipline);
        tvDiscipline.setText(getGroup(position).discipline_str);
        TextView tvRegion = (TextView) convertView.findViewById(R.id.row_region);
        tvRegion.setText(getGroup(position).region);

        RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rl_row);
        switch (getGroup(position).sport_str)
        {
            case "TRAIL": tvSport.setBackgroundColor(Color.YELLOW);break;
            case "MTBO": tvSport.setBackgroundColor(Color.rgb(255,100,0));break;
            case "LOB": tvSport.setBackgroundColor(Color.BLUE);break;
            case "OB": tvSport.setBackgroundColor(Color.GREEN);break;
            default: tvSport.setBackgroundColor(Color.WHITE);break;
        }

        if (position % 2 == 1) {
            rl.setBackgroundResource(R.drawable.border_grey);
        } else {
            rl.setBackgroundResource(R.drawable.border);
        }
        rl.setTag(position);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("ExpandableListAdapter", "Zobrazit detail");
                showEventDetails((Integer)v.getTag());
            }
        });

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_details, null);
        }

		// Nastaveni hodnot v podradku
		TextView region = (TextView) convertView.findViewById(R.id.row_region);
		region.setText(getGroup(groupPosition).region);
		TextView sport = (TextView) convertView.findViewById(R.id.row_sport);
		sport.setText(getGroup(groupPosition).sport_str);
		TextView discipline = (TextView) convertView.findViewById(R.id.row_discipline);
		discipline.setText(getGroup(groupPosition).discipline_str);
        TextView level = (TextView) convertView.findViewById(R.id.row_level);
        level.setText(getGroup(groupPosition).level_str);

		ImageView rowPlus = (ImageView) convertView.findViewById(R.id.row_plus);
		rowPlus.setTag(groupPosition);
		rowPlus.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        Log.i("ExpandableListAdapter", "Zobrazit detail");
		        showEventDetails((Integer)v.getTag());
		    }
		});

		return convertView;
	}
	
//	 @Override
//	 public void onGroupCollapsed(int groupPosition) {
//	   super.onGroupCollapsed(groupPosition);
//	 }

//	 @Override
//	 public void onGroupExpanded(int groupPosition) {
//	   super.onGroupExpanded(groupPosition);
//	 }

//	@Override
//	public boolean isChildSelectable(int groupPosition, int childPosition) {
//		return true;
//	}
	
	protected void showEventDetails(int groupPosition){
		EventFragment eventDetail = new EventFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id", getGroup(groupPosition).id);		
		eventDetail.setArguments(bundle);
		
		FragmentManager fragmentManager = ((Activity) mContext).getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.content_frame, eventDetail);
		transaction.addToBackStack(null);
		transaction.commit();		
	}

    @Override
    public boolean isEnabled(int position) {
        return true;
    }
}
