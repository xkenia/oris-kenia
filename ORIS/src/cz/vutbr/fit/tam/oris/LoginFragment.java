package cz.vutbr.fit.tam.oris;

import org.json.JSONObject;

import cz.vutbr.fit.tam.oris.OrisAPI.Method;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginFragment extends Fragment implements OrisAPIListener {

	//TODO Vytvořit checkButton pro "zapamatovat přihlášení" a implementovat SharedPreferences
	
	/** Konstruktor */
	public LoginFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Toast.makeText(getActivity().getApplicationContext(), "Přihlášení v této verzi nefunguje", Toast.LENGTH_SHORT).show();
		
		View loginView = inflater.inflate(R.layout.fragment_login, container, false);
		
		Button login = (Button) loginView.findViewById(R.id.bLogin);
		//TODO smazat po doimplementaci přihlašování
		login.setEnabled(false);
		TextView tv = (TextView) loginView.findViewById(R.id.password);
		tv.setEnabled(false);
		tv = (TextView) loginView.findViewById(R.id.login);
		tv.setEnabled(false);
		// po sem smazat
		login.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v) {
				Toast.makeText(getActivity().getApplicationContext(), "Stisknuto Přihlásit", Toast.LENGTH_SHORT).show();
			}
		});

		
		getActivity().setTitle("Přihlášení");
		return loginView;
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_order_by).setVisible(false);
        menu.findItem(R.id.action_order).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

	@Override
	public void onDownloadComplete(JSONObject data, Method method) {

	}

}
