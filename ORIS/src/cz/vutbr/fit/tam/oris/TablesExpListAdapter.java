package cz.vutbr.fit.tam.oris;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/** 
 * Trida se stara o zobrazeni seznamu prihlasek/vysledku po kategoriich.
 * Jde o ExpandableList, kde Group je kategorie, ktera ma pouze jedno Child,
 * coz je tabulka s prihlasky/vysledky.
 */
public class TablesExpListAdapter extends BaseExpandableListAdapter {
	
	/** Kontext aplikace */
	private Context mContext;
	/** Seznam tabulek */
	private List<ClassTable> mExpList;	
	
	/**
	 * Konstruktor.
	 * Preda se mu seznam tabulek po kategoriich pro vykresleni.
	 * @param context
	 * @param entries
	 */
	public TablesExpListAdapter(Context context, List<ClassTable> entries) {
		this.mContext = context;
		this.mExpList = entries;		
	}

	/**
	 * Pocet kategorii.
	 */
	@Override
	public int getGroupCount() {
		return this.mExpList.size();
	}

	/**
	 * Pocet potomku (pouze jedna tabulka pro kategorii).
	 */
	@Override
	public int getChildrenCount(int groupPosition) {		
		return 1;
	}
	
	/**
	 * Metoda vraci tabulku a popis pro danou kategorii ze seznamu.
	 * 
	 * @param groupPosition Aktualni pozice v seznamu
	 * @return Vrati se tabulka + popis kategorie.
	 * @see ClassTable
	 */
	@Override	
	public ClassTable getGroup(int groupPosition) {
		return this.mExpList.get(groupPosition);
	}

	/**
	 * Metoda vraci tabulku pro vykresleni.
	 * @param groupPosition Aktualni pozice v seznamu
	 * @param childPosition Aktualni pozice v Group (vzdy 1)
	 * @param Tabulka
	 */
	@Override
	public List<List<String>> getChild(int groupPosition, int childPosition) {
		return this.mExpList.get(groupPosition).table;
		
	}

	/**
	 * Metoda id aktualni pozice.
	 * @param groupPosition Aktualni pozice v seznamu
	 */
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	/**
	 * Metoda id pozici potomka.
	 * @param groupPosition Aktualni pozice v seznamu
	 * @param childPosition Aktualni pozice v Group (vzdy 1)
	 * @return ID
	 */
	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	/**
	 * Metoda vytvori radek v seznamu.
	 * Prida se nazev kategorie a jeho popisek.
	 * @param groupPosition Aktualni pozice v seznamu
	 * @param isExpanded Priznak zda je seznam rozbaleny
	 * @param convertView 
	 * @param parent
	 * @return View polozky seznamu
	 */
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.result_row, null);
		}
		
		TextView className = (TextView) convertView.findViewById(R.id.resultRowName);
		className.setText(getGroup(groupPosition).category.className);		
		TextView classDescription = (TextView) convertView.findViewById(R.id.resultRowDescription);
		classDescription.setText(getGroup(groupPosition).category.getDescription());
		
		if (groupPosition % 2 == 1) {
			LinearLayout rl = (LinearLayout) convertView.findViewById(R.id.res_row);
			rl.setBackgroundResource(R.drawable.border_grey);
		} else {
			LinearLayout rl = (LinearLayout) convertView.findViewById(R.id.res_row);
			rl.setBackgroundResource(R.drawable.border);
		}
		
		return convertView;
	}

	/**
	 * Vola se pri rozbaleni polozky seznamu.
	 * Prida do do rozbalene casti tabulku s prihlaskami/vysledky
	 * @param groupPosition Aktualni pozice v seznamu
	 * @param childPosition Aktualni pozice v Group (vzdy 1)
	 * @param isLastChild Priznak zda jde o posledniho potomka v rozbalene casti
	 * @param convertView 
	 * @param parent
	 * @return  View potomka
	 */
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		// podle pozic v ExpandableList vybere ze seznamu danou tabulku
		List<List<String>> classTable = getChild(groupPosition, childPosition);
		
		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.result_row_detail, null);
        }
		
		TableLayout table = (TableLayout) convertView.findViewById(R.id.resultsTable);		
		
		// pokud jiz obsahuje nejakou tabulku tak ji smaze
		int count = table.getChildCount();
		for (int i = 0; i < count; i++) {
		    View child = table.getChildAt(i);
		    if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
		}
		
		// vytvoreni hlavicky
		TableRow head = new TableRow(this.mContext);
		head.setBackgroundColor(this.mContext.getResources().getColor(R.color.grey_border));
		TableLayout.LayoutParams llp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT);
	    head.setLayoutParams(llp);
	    for(int i = 0; i < classTable.get(0).size(); i++){		    	
	    	if(i==1)
	    		addColumnHead(head, classTable.get(0).get(i),1);
	    	else
	    		addColumnHead(head, classTable.get(0).get(i),0);
	    }
		table.addView(head);
		
		// vytvoreni radku
		for(int rowNum=1; rowNum < classTable.size(); rowNum++){
			TableRow row = new TableRow(this.mContext);
			row.setBackgroundColor(this.mContext.getResources().getColor(R.color.grey_border));
			llp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT);
		    row.setLayoutParams(llp);
			for(int col = 0; col < classTable.get(rowNum).size(); col++){					
				if(col==1)
					addColumn(row, classTable.get(rowNum).get(col),1);					
				else
					addColumn(row, classTable.get(rowNum).get(col),0);
			}
			table.addView(row);
		}	
	
		
		return convertView;
	}
	
	/**
	 * Nastaveni atributu a pridani sloupce do radku.
	 * Do TableRow se prida TextView.
	 * @param row Radek tabulky
	 * @param colValue Hodnota bunky
	 * @param weight Vaha pro roztazeni bunky v radku
	 */
	protected void addColumn(TableRow row, String colValue, float weight){
		TextView col = new TextView(this.mContext);
		col.setText(colValue);
		col.setBackgroundColor(Color.WHITE);
		col.setPadding(10, 1, 10, 1);
	    
		TableRow.LayoutParams llp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
	    llp.setMargins(1, 1, 1, 1); 
	    llp.weight=weight;
	    col.setLayoutParams(llp);
	    
		row.addView(col);
	}
	
	/**
	 * Nastaveni atributu a pridani sloupce do hlavicky.
	 * Do TableRow se prida TextView.
	 * @param row Radek tabulky
	 * @param colValue Hodnota bunky
	 * @param weight Vaha pro roztazeni bunky v hlavicce
	 */
	protected void addColumnHead(TableRow row, String colValue, float weight){
		TextView col = new TextView(this.mContext);
		col.setText(colValue);	
		col.setPadding(10, 1, 10, 1);
	    
		TableRow.LayoutParams llp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
	    llp.setMargins(1, 1, 1, 1); 
	    llp.weight=weight;
	    col.setLayoutParams(llp);
	    
		row.addView(col);
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}
