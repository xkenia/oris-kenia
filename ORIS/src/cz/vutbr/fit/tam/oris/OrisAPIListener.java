package cz.vutbr.fit.tam.oris;

import org.json.JSONObject;

/** Interface, ktery deklaruje metodu volanou pri prijeti vysledku ze serveru. */
public interface OrisAPIListener {
	/**Metoda vraci data z odpovedi a typ pozadavku 
	 * @param data odpoved serveru, prevedena na JSONObject
	 * @param method typ dotazu na server
	 * */
	void onDownloadComplete(JSONObject data, OrisAPI.Method method);

}
