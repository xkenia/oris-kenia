/**
 * 
 */
package cz.vutbr.fit.tam.oris;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;


/** Zajistuje komunikaci s webovym API. Pri dotazu na server se spusti vlakno pro odeslani pozadavku
 * a stazeni odpovedi. Po odpovedi serveru je volana metoda z onDownloadComplete() OrisAPIListener, 
 * kde preda vysledek.
 * Pro pouziti je potreba svoji tridu implementovat jako intreface OrisAPIListener 
 * s metodou onDownloadComplete().
 */
public class OrisAPI{

	private static final String DEBUG_TAG = "HttpExample";
	/** Kontext aktivity */
	Context context;
	/** Listener aktivity */
	OrisAPIListener callback;
	
	/** Vycet dotazu na API. Vraci se spolecne s vysledkem. */
	public enum Method{
		GET_EVENT_LIST,
		GET_EVENT,
		GET_EVENT_ENTRIES,
		GET_EVENT_SERVICE_ENTRIES,
		GET_EVENT_RESULTS,
		GET_USER,
		GET_REGISTRATION,
		GET_CSOS_CLUB_LIST,
		GET_CLUB,
		UNABLE_RETRIEVE_DATA
	}
	
	/** Vycet druhu sportu */
	public enum Sport{
		ALL_SPORTS,
		OB,
		LOB,
		MTBO
	}
	
	/*public enum Level{
		ALL,
		MCR,
		ZA,
		ZB,
		V,
		CPS,
		CP		
	}*/
	
	/** Listener pro vysledek z vlakna, ktere ziska data od weboveho API.
	 * Preposilaji se do aktivity, ktera pracuje vysledek.
	 */
	DataDownloaderListener DCallback= new  DataDownloaderListener(){
        @Override
        public void onDownloadComplete(String data,Method method) {
        	JSONObject jsonData = null;
        	//Log.d(DEBUG_TAG,data);
        	switch(method){
        		case GET_EVENT_LIST:
        		case GET_EVENT:
        		case GET_CLUB:
        		case GET_CSOS_CLUB_LIST:
    			case GET_EVENT_ENTRIES:
    			case GET_EVENT_RESULTS:    				
    			case GET_EVENT_SERVICE_ENTRIES:    				
    			case GET_REGISTRATION:    				
    			case GET_USER:
        			// vytvoreni JSONObjectu z prijatych dat.
	        		try {
	        			if (data != null) {
	        				jsonData = new JSONObject(data);     	
	        			}
		     		}      		
		     		catch (JSONException e) {
		     			e.printStackTrace();
		     		}
        		break;	
        		case UNABLE_RETRIEVE_DATA:
        		break;			
        	}	
	        callback.onDownloadComplete(jsonData, method);
        }
    };	
	
    /** Konstruktor
     * @param mContext kontext aktivity
     * @param mCallback callback aktivity
     */
	public OrisAPI(Context mContext, OrisAPIListener mCallback){				
		this.context = mContext;
		this.callback = mCallback;
	}
	
	/** Interface, ktery deklaruje metodu volanou z pracovniho vlakna pri prijeti vysledku ze serveru. */
	protected interface DataDownloaderListener{
		/**Metoda vraci data z odpovedi a typ pozadavku 
		 * @param data odpoved serveru
		 * @param method typ dotazu na server
		 * */
		public void onDownloadComplete(String data,Method method);
	}	
	
	/** Interface, ktery deklaruje metodu volanou z pracovniho vlakna pri prijeti vysledku ze serveru. */
	/*protected interface DataInsertListener{
		
		//public void onDownloadComplete(String data,Method method);
		public void onInsertComplete();
	}*/
	
	
	/** Dotaz na seznam zavodu.
	 * @param sport 		id sportu
	 * @param rg 			zkratka regionu
	 * @param name 			cast nazvu zavodu
	 * @param dateFrom 		datum od ve form�tu RRRR-MM-DD
	 * @param dateTo 		datum do ve form�tu RRRR-MM-DD
	 * @throws Exception 	Vyhodi se vyjimka pokud se nelze pripojit k siti.
	 */
	public void getEventList(Sport sport, String rg, String name, String dateFrom, String dateTo, boolean allEvents) throws Exception{
		//String request = new String("http://oris.orientacnisporty.cz/API/?format=json&method=getEventList");
		String request = new String("format=json&method=getEventList");
		if(allEvents){
			request += "&all=1";
		}
		
		if(sport != Sport.ALL_SPORTS)	{		
			switch(sport){
			case OB:
				request += "&sport=1";
				break;
				
			case LOB:
				request += "&sport=2";
				break;
				
			case MTBO:
				request += "&sport=3";
				break;
			default:
				break;
			}
			
		}
		/*if(level != Level.ALL)	{		
			switch(level){
			case MCR:
				request += "&level=1";
				break;
				
			case OF:
				request += "&level=2";
				break;
				
			case OZ:
				request += "&level=3";
				break;
			default:
				break;
			}
			
		}*/
		if(rg.length() != 0)			
			request += "&rg="+rg;		
		if(name.length() != 0)			
			request += "&name="+name;
		if(dateFrom.length() != 0){	
			if(!checkDate(dateFrom))
				throw new Exception("�patn� form�t data.");
			
			request += "&datefrom="+dateFrom;
			//Log.d(DEBUG_TAG,"dateFrom ok");
		}
		if(dateTo.length() != 0){
			if(!checkDate(dateTo))
				throw new Exception("�patn� form�t data.");
			
			request += "&dateto="+dateTo;
			//Log.d(DEBUG_TAG,"dateTo ok");
		}
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_LIST);
	}
	
	public void getEvent(String id) throws Exception{
	
		String request = new String("format=json&method=getEvent");
		
		if(id.length() != 0 )
			request += "&id="+id;
		else
			throw new Exception("Mus� b�t vybr�n z�vod.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT);
	}
	
	public void getEventEntries(String eventId, String classId, String className, String clubId, String entryStop, String entryStopOut) throws Exception{
		String request = new String("format=json&method=getEventEntries");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Mus� b�t vybr�n z�vod.");
		
		if((clubId.length() != 0) && ((classId.length() != 0) || (className.length() != 0))){
			throw new Exception("Nelze sou�asn� filtrovat p�ihl�ky podle klubu a kategorie.");
		}
		if(classId.length() != 0)
			request += "&classid="+classId;
		if(className.length() != 0)
			request += "&classname="+className;
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		if(entryStop.length() != 0)
			request += "&entrystop="+entryStop;
		if(entryStopOut.length() != 0)
			request += "&entrystopout="+entryStopOut;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_ENTRIES);
	}
	
	public void getEventResults(String eventId, String classId, String className, String clubId) throws Exception{
		String request = new String("format=json&method=getEventResults");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Mus� b�t vybr�n z�vod.");
		
		if((clubId.length() != 0) && ((classId.length() != 0) || (className.length() != 0))){
			throw new Exception("Nelze sou�asn� filtrovat v�sledky podle klubu a kategorie.");
		}
		if(classId.length() != 0)
			request += "&classid="+classId;
		if(className.length() != 0)
			request += "&classname="+className;
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_RESULTS);
	}	
	
	public void getEventServiceEntries(String eventId, String clubId) throws Exception{
		String request = new String("format=json&method=getEventServiceEntries");
		if(eventId.length() != 0 )
			request += "&eventid="+eventId;
		else
			throw new Exception("Mus� b�t vybr�n z�vod.");		
		
		if(clubId.length() != 0)
			request += "&clubid="+clubId;
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_EVENT_SERVICE_ENTRIES);
	}
	
	public void getUser(String rgNum) throws Exception{
		String request = new String("format=json&method=getUser");
		if(rgNum.length() != 0 )
			request += "&rgnum="+rgNum;
		else
			throw new Exception("Mus� b�t zad�no registra�n� ��slo.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_USER);
	}
	
	
	public void getRegistration(Sport sport, String year) throws Exception{
		String request = new String("format=json&method=getRegistration");
				
		switch(sport){				
		case OB:
			request += "&sport=1";
			break;
			
		case LOB:
			request += "&sport=2";
			break;
			
		case MTBO:
			request += "&sport=3";
			break;
			
		default:
			throw new Exception("Nen� vybr�n sport.");
		}
		
		if(year.length() == 4 )
			request += "&year="+year;
		else
			throw new Exception("Nen� vybr�n rok.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_REGISTRATION);
	}	
		
	public void getCSOSClubList() throws Exception{
		String request = new String("format=json&method=getCSOSClubList");	
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_CSOS_CLUB_LIST);
	}
	
	public void getClub(String clubId) throws Exception{
		String request = new String("format=json&method=getClub");			
		
		if(clubId.length() != 0)
			request += "&id="+clubId;
		else
			throw new Exception("Nen� vybr�n club.");
		
		String url = makeURL(request);
		this.downloadData(url, Method.GET_CLUB);
	}
	
	/** Vytvoreni http dotazu.  
	 * @param request Fragment dotazu
	 * @throws URISyntaxException 
	 */
	protected String makeURL(String request) throws URISyntaxException{		
		URI uri = new URI(
    		    "http", 
    		    "oris.orientacnisporty.cz", 
    		    "/API/",
    		    request,
    		    null
    		    );
		
		//Log.d(DEBUG_TAG,uri.toASCIIString());
		return uri.toASCIIString();
	}
	
	/** Kotrola formatu data.  
	 * @param date datum
	 */
	protected boolean checkDate(String date){		
		return Pattern.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]",date);
	}
	
	/** Kotrola formatu data.
	 * @param url http dotaz  
	 */
	protected void downloadData(String url, Method method) throws Exception{
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {	
			DataDownloader downloader = new DataDownloader(this.DCallback, method);		    		    	 				    	
			downloader.execute(url);					
		   //	Log.d(DEBUG_TAG,"Network ok.");
		} else {
			//Log.d(DEBUG_TAG,"No network connection available.");
			throw new Exception("Není k dispozici internetové připojení.");		 
		}
	}
	
	/** Trida posila pozadavek a prijima odpovede ze serveru ORIS. Je spoustena ve vlastnim vlakne, 
	 * takze nezamrzne gui. Pri prijimani pozadavku zobrazi progres bar
	 */
	private class DataDownloader extends AsyncTask<String, Void, String> {
		
		/** Progres dialog pri stahovani */
		ProgressDialog progressDialog;
		/** Listener tridy OrisAPI */
		DataDownloaderListener callback;
		/** Typ dotazu na server */
		Method method;
		
		/** Konstruktor	    * 
	    * @param mCallback Callback aktivity
	    * @param mMethod Typ dotazu na server
	    */
		public DataDownloader(DataDownloaderListener mCallback, Method mMethod){
			this.callback = mCallback;
			this.method = mMethod;
		}
		
		/** Spousti metodu v novem vlakne.
		* @param url http dotaz
		*/
        @Override
        protected String doInBackground(String ... url) { 
        	// pokud se nepodari stazeni nastavi se method do UNABLE_RETRIEVE_DATA,
        	// ktere se pak dostane do aktivity.
            try {            	
            	return downloadUrl(url[0]);
            } catch (IOException e) {
            	method = Method.UNABLE_RETRIEVE_DATA;
                return "Unable to retrieve web page. URL may be invalid.";
            }            
        }        
        
        /** Pred spustenim prace v novem vlakne zobrazi progress bar.*/		
        @Override
        protected void onPreExecute() {
        	if (progressDialog == null) {
        		progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(context.getResources().getString(R.string.downloading));
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
        	}
        }
        
        /** Po stazeni dat zrusi progress bar a vola callback v OrisAPI.
		* @param result odpoved serveru
		*/
        @Override
        protected void onPostExecute(String result) {        	      	
        	callback.onDownloadComplete(result, this.method);
        	if (progressDialog.isShowing()) {
        		progressDialog.dismiss();
            }
        }
        
        /** Po stazeni dat zrusi progress bar a vola callback v OrisAPI.
		* @param urlString http dotaz
		* @throws IOException chyba pri spojeni k serveru
		*/
        protected String downloadUrl(String urlString) throws IOException {        	
            BufferedInputStream in = null;
            try { 
                URL url = new URL(urlString);
                
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();               
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();                
                in = new BufferedInputStream(conn.getInputStream());
                
                Log.d(DEBUG_TAG, "The response is: " + response);
                
                // Prevedeni InputStream na String 
                String data = readData(in);                
                return data;
            } finally {
                if (in != null) {
                    in.close();
                }                
            }
        }
        
        /** Prevedeni prijatych dat na string.
		* @param stream Odpoved serveru.
		* @throws IOException chyba pri spojeni k serveru
		*/
        protected String readData(BufferedInputStream stream) throws IOException {
        	byte[] content = new byte[1024];

        	int bytesRead=0;
        	String strData = new String(""); 
        	while( (bytesRead = stream.read(content)) != -1){         		 
        		strData += new String(content, 0, bytesRead);
        	}
            return strData;
        }
    }
}
